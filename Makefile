
.PHONY: all clean

all: main.pdf

clean:
	rm -rf .cache
	rm -f main.pdf

main.pdf: .cache src/main.tex src/prelude.tex
	cp -vu -a src/* .cache/
	cd .cache; latexmk -pdf -halt-on-error main.tex
	cp -vu .cache/main.pdf main.pdf

.cache:
	mkdir -p .cache
