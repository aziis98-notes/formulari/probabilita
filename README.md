
# Formulario di Probabilità

Versione più recente del PDF: <https://gitlab.com/aziis98-notes/formulari/probabilita/-/raw/main/main.pdf>

Generato dal template: <https://github.com/aziis98/template-latex>

## Usage

Just run `make`, `make all` or `make main.pdf` to build the project and generate the output PDF.

The alternative is to run [`./watch`](./watch) that uses `entr` to watch and rerender the PDF when the source files change.
